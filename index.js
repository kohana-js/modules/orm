require('kohanajs').addNodeModule(__dirname);

module.exports={
  ControllerMixinORMInput : require('./classes/controller-mixin/ORMInput'),
  ControllerMixinORMRead: require('./classes/controller-mixin/ORMRead'),
  ControllerMixinORMWrite : require('./classes/controller-mixin/ORMWrite'),
  ControllerMixinORMDelete: require('./classes/controller-mixin/ORMDelete'),
}