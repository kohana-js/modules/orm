const {ORM} = require('kohanajs');

class ModelTest extends ORM{
  name=null;
  enabled = true;

  static joinTablePrefix = 'test';
  static tableName = 'tests';

  static fields = new Map([
    ["name", "String!"],
    ["enabled", "Boolean!"],
  ]);
}

module.exports = ModelTest;
