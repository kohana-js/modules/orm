const {ORM} = require('kohanajs');

class ModelParent extends ORM{
  name=null;
  static joinTablePrefix = 'parent';
  static tableName = 'parents';

  static fields = new Map([
    ["name", "String!"]
  ]);

  static hasMany = [
    ["child_id", "ModelChild"]
  ];
}

module.exports = ModelParent;
