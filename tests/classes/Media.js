const {ORM} = require('kohanajs');

class Media extends ORM{
  url=null;
  static joinTablePrefix = 'media';
  static tableName = 'media';

  static fields = new Map([
    ["url", "String!"],
  ]);
}

module.exports = Media;
