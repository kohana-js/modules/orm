const path = require('path');
const qs = require('qs');
const {KohanaJS, Controller} = require('kohanajs');
KohanaJS.init();
KohanaJS.classPath.set('helper/Form.js', path.normalize(__dirname + '/../classes/helper/Form.js'));


const ControllerMixinORMInput = require('../classes/controller-mixin/ORMInput');

const Person = require('./classes/model/Person');
const User = require('./classes/model/User');
const Role = require('./classes/model/Role');
const Tag = require('./classes/model/Tag');

KohanaJS.classPath.set('model/Person.js', Person);
KohanaJS.classPath.set('model/User.js', User);
KohanaJS.classPath.set('model/Role.js', Role);
KohanaJS.classPath.set('model/Tag.js', Tag);

class ControllerTest extends Controller{
  constructor(request, Model) {
    super(request);
    this.model = Model;

    const $_POST = (typeof this.request.body === 'object') ? this.request.body : qs.parse(this.request.body);
    this.state.set(ControllerMixinORMInput.POST, $_POST);

    ControllerTest.mix(this, [ControllerMixinORMInput])
  }

  async action_index(){
    this.body = 'index';
  }

  async action_update(){
    this.body = {
      update: this.state.get(ControllerMixinORMInput.ORM_INPUT)
    }
  };
}

describe('orm input parse', function () {
  test('init', async ()=>{
    const c = new ControllerTest({headers:{}, params:{}, body:{}}, Person);
    const result = await c.execute();
    if(result.status !== 200)console.log(result);
    expect(result.status).toBe(200);
    expect(result.body).toBe('index');
  });

  test('currentField, new instance', async ()=>{
    const c = new ControllerTest({headers: {}, params:{}, body:':first_name=Alice&:Tag[]=10&:Tag[]=12&:last_name=Lee' }, Person)
    const result = await c.execute('update');
    if(result.status !== 200)console.log(result);
    expect(result.status).toBe(200);

    const m1 = result.body.update.get('Person').get('?');
    expect(m1).toBeDefined();
    expect(m1.get('first_name')).toBe('Alice');
    expect(m1.get('last_name')).toBe('Lee');
    expect(m1.get('Tag').join()).toBe([10, 12].join());

  });

  test('currentField', async ()=>{
    const c = new ControllerTest({headers: {}, params:{"id":1}, body:':first_name=Alice&:Tag[]=10&:Tag[]=12&:last_name=Lee'}, Person)
    const result = await c.execute('update');
    if(result.status !== 200)console.log(result);
    expect(result.status).toBe(200);

    const m1 = result.body.update.get('Person').get(1);
    expect(m1.get('first_name')).toBe('Alice');
    expect(m1.get('last_name')).toBe('Lee');
  });

  test('currentField, multiple instance', async ()=>{
    const c = new ControllerTest({headers: {}, params:{"id":1}, body:':first_name=Alice&(10):last_name=Lee&>User():username=alicelee001&>User(100):username=alicelee'}, Person)
    const result = await c.execute('update');
    if(result.status !== 200)console.log(result);
    expect(result.status).toBe(200);

    const m1 = result.body.update.get('Person').get(1);
    expect(m1.get('first_name')).toBe('Alice');

    const m2 = result.body.update.get('Person').get(10);
    expect(m2.get('last_name')).toBe('Lee');
  });

  test('children, with id', async ()=>{
    const c = new ControllerTest({headers: {}, params:{"id":1}, body:':first_name=Alice&(10):last_name=Lee&>User():username=alicelee001&>User(100):username=alicelee'}, Person)
    const result = await c.execute('update');
    if(result.status !== 200)console.log(result);
    expect(result.status).toBe(200);

    const u = result.body.update.get('User');
    const u1 = u.get(100);
    expect(u1.get('username')).toBe('alicelee')

    const nu = u.get('?');
    expect(nu.get('username')).toBe('alicelee001');
  });

  test('children, new Model', async ()=>{
    const c = new ControllerTest({headers: {}, params:{}, body:':first_name=Alice&>User():username=alicelee001&(50)>User(4):username=bobo'}, Person)
    const result = await c.execute('update');
    if(result.status !== 200)console.log(result);
    expect(result.status).toBe(200);

    const u = result.body.update.get('User');
    const u1 = u.get('?');
    expect(u1.get('.person_id:Person')).toBe('?');
    expect(u1.get('username')).toBe('alicelee001');

    const u2 = u.get(4);
    expect(u2.get('.person_id:Person')).toBe(50);
    expect(u2.get('username')).toBe('bobo')
  });

  test('children, explicit fk', async ()=>{
    const c = new ControllerTest({headers:{}, params:{"id":1}, body:'>User(4)-person_id:username=coco&>User(5)-supervisor_id:username=soso&>User()-supervisor_id:username=fofo&>User():username=koko'}, Person);
    const result = await c.execute('update');
    if(result.status !== 200)console.log(result);
    expect(result.status).toBe(200);

    const u = result.body.update.get('User');
    const u1 = u.get(4);
    expect(u1.get('.person_id:Person')).toBe(1);
    expect(u1.get('username')).toBe('coco');

    const u2 = u.get(5);
    expect(u2.get('.supervisor_id:Person')).toBe(1);
    expect(u2.get('username')).toBe('soso');

    const u3 = u.get('?');
    expect(u3.get('.supervisor_id:Person')).toBe(1);
    expect(u3.get('username')).toBe('koko');
  });

  //recursive children?

  test('siblings', async ()=>{
    const c = new ControllerTest({headers:{}, params:{"id":1}, body:':Tag[]=1&:Tag[]=2&:Tag[]=3&*Tag[]=8&*Tag[]=7&(20)*Tag[]=3&(20)*Tag[]=4&(20):Tag[]=1&(20):Tag[]=2'}, Person);
    const result = await c.execute('update');
    if(result.status !== 200)console.log(result);
    expect(result.status).toBe(200);
    const p = result.body.update.get('Person');
    const p1 = p.get(1);
    expect(p1.get('Tag').sort().join()).toBe([1,2,3].join());
    expect(p1.get('*Tag').sort().join()).toBe([7,8].join());
  })

  test('inverse siblings', async ()=>{
    /*Person belongsMany Tags,  */
    const c = new ControllerTest({headers:{}, params:{"id":1}, body:':name=foo&:Person[]=1&:Person[]=2&*Person[]=3&*Person[]=4'}, Tag);
    const result = await c.execute('update');
    if(result.status !== 200)console.log(result);
    expect(result.status).toBe(200);

    const t = result.body.update.get('Tag');
    const t1 = t.get(1);
    expect(t1.get('Person').sort().join()).toBe([1,2].join());
    expect(t1.get('*Person').sort().join()).toBe([3,4].join());
  })

  test('invalid siblings', async () => {
    /*Person belongsMany Tags,  */
    const c = new ControllerTest({headers:{}, params:{"id":1}, body:':User[]=1&:User[]=2&*User[]=3&*User[]=4'}, Tag);
    const result = await c.execute('update');
    expect(result.status).toBe(500);
    expect(c.error.message).toBe('Invalid input siblings User for Tag');
  })

  test('invalid addvsiblings', async () => {
    /*Person belongsMany Tags,  */
    const c = new ControllerTest({headers:{}, params:{"id":1}, body:'*User[]=3&*User[]=4'}, Tag);
    const result = await c.execute('update');
    expect(result.status).toBe(500);
    expect(c.error.message).toBe('Invalid hasAndBelongsToMany User and Tag');
  })

  test('parse action create person', async ()=>{
    const c = new ControllerTest({headers:{}, params:{}, body: {
        ':first_name' : 'Bob',
        ':last_name' : 'Chan',
        '>User():username' : 'bobbob',
        '>User():password' : '#11111111',
      }}, Person);
    const result = await c.execute('update');
    if(result.status !== 200)console.log(result);
    expect(result.status).toBe(200);
  });

  test('coverage test, Invalid input field ${prop} for ${Model.name}', async () => {
    const c = new ControllerTest({
      headers: {}, params: {}, body: {
        ':xxx' : 'not available',
      }
    }, Person)
  })
});