const {KohanaJS, ORM, ControllerMixin, dereference:$} = require('kohanajs');

const mapGetOrCreate = (map, key, defaultValue) => {
  const result = map.get(key);

  if(!result){
    map.set(key, defaultValue);
    return defaultValue;
  }

  return result;
}

/**
 *
 * @param {string} code
 * @param {number| string} currentID
 * @param {string} prefix
 * @returns {number|string}
 */
const parseModelId = (code, currentID) => {
  if(!code || code==='()')return currentID;
  return parseInt(code.replace(/[^0-9]/g, ''));
}

const parseFK = (code, defaultFK) => {
  if(!code || code==='-')return defaultFK;
  return code.replace(/^-/, '');
}

const parseClassField = (result, postData, Model, currentID='?') =>{
  const pattern = /^(\(\d+\))?:(\w+)$/;

  postData.forEach((v, key)=>{
    const matches = pattern.exec(key)
    if(!matches)return;

    const type = Model.name;
    const id   = parseModelId(matches[1], currentID);
    const prop = matches[2];
    const value = Array.isArray(v) ? v.map(x => parseInt(x)): v;

    if(Array.isArray(v)) {
      const Type = KohanaJS.require(ORM.classPrefix+prop);
      if(!Model.belongsToMany.has(prop) && !Type.belongsToMany.has(type))throw new Error(`Invalid input siblings ${prop} for ${Model.name}`);
    }else{
      if(!Model.fields.has(prop) && !Model.belongsTo.has(prop)){
        throw new Error(`Invalid input field ${prop} for ${Model.name}`);
      }
    }

    const instancesType = mapGetOrCreate(result, type, new Map());
    const instanceTypeId = mapGetOrCreate(instancesType, id, new Map());
    if(id === '?' && value === ''){
      postData.delete(key)
      return;
    }

    instanceTypeId.set(prop, value);
    postData.delete(key);
  })
  return result
}

const parseChildField = (result, postData, Model, currentID='?') => {
  const pattern =/^(\(\d+\))?>(\w+)(\(\d*\))?(-\w+)?:(\w+)$/
  postData.forEach((v, key)=>{
    const matches = pattern.exec(key)
    if(!matches)return;

    const parentID = parseModelId(matches[1], currentID);

    const type = matches[2];
    const id   = parseModelId(matches[3], '?');
    if(id !== '?' && parentID === '?')throw new Error(`${type}(${id}) cannot own by new ${Model.name}() `);

    const fk   = parseFK( matches[4], Model.joinTablePrefix+'_id');
    const prop = matches[5];
    const value = Array.isArray(v) ? v.map(x => parseInt(x)): v;

    const Type = ORM.require(type);
    if(!Type.fields.has(prop) && !Type.belongsTo.has(prop))throw new Error(`Invalid input field ${prop} for ${Type.name}`);
    if(!Type.belongsTo.has(fk))throw new Error(`Invalid FK ${fk} for ${Type.name}`);

    const instancesType = mapGetOrCreate(result, type, new Map());
    const instanceTypeId = mapGetOrCreate(instancesType, id, new Map([[`.${fk}:${Model.name}`, parentID]]));
    if(value !== '') instanceTypeId.set(prop, value);

    postData.delete(key);
  });
}

const parseAddSibling = (result, postData, Model, currentID='?')=>{
  const pattern = /^(\(\d+\))?\*(\w+)$/;

  postData.forEach((v, key)=>{
    const matches = pattern.exec(key);
    if(!matches)return;
    if(!Array.isArray(v))throw new Error(`${key} must be array`);

    const type = matches[2];
    const id   = parseModelId(matches[1], currentID);
    const prop = '*'+matches[2];
    const value = v.map(x => (x === 'replace')? x : parseInt(x));

    const Type = ORM.require(type);
    if(!Model.belongsToMany.has(type) && !Type.belongsToMany.has(Model.name)){
      throw new Error(`Invalid hasAndBelongsToMany ${type} and ${Model.name}`);
    }

    const instancesType = mapGetOrCreate(result, Model.name, new Map());
    const instanceTypeId = mapGetOrCreate(instancesType, id, new Map());
    instanceTypeId.set(prop, value);

    postData.delete(key);
  })
  return result
}

//parse form input to update list, with validate
class ORMInput extends ControllerMixin{
  static ORM_INPUT = 'ORMInput';
  static POST = '$_POST';

  static async action_update(state){
    const client = state.get('client');
    const model = client.model;

    const id = client.request.params.id
    const $_POST = state.get(this.POST);
    const postData = new Map(Object.entries($_POST));
    //parse postData;
    //find instance fields
    const updates = new Map();

    parseClassField(updates, postData, model, id);
    parseChildField(updates, postData, model, id);
    parseAddSibling(updates, postData, model, id);

    state.set(this.ORM_INPUT, updates);
  }

}

module.exports = ORMInput;