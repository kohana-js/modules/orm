const {ORM, ControllerMixin, ControllerMixinDatabase} = require('kohanajs');

class ControllerMixinORMRead extends ControllerMixin{
  static OPTIONS = 'orm_read_options';

  static INSTANCES = 'instances';
  static INSTANCE = 'instance';
  static COUNT = 'count';
  static DATABASE_KEY = 'ormDatabaseKey';

  static init(state){
    state.set(this.OPTIONS, Object.assign({
      orderBy: new Map([['id', 'ASC']]),
      limit: 50
    }, state.get(this.OPTIONS) ));

    state.set(this.DATABASE_KEY, state.get(this.DATABASE_KEY) || 'admin');

//    this.id = this.request.params?.id ? parseInt(this.request.params.id) : null;
  }

  static async action_index(state){
    const client = state.get(ControllerMixin.CLIENT);
    const model = client.model;

    if(!model)return;
    const database = state.get(ControllerMixinDatabase.DATABASES).get(state.get(this.DATABASE_KEY));
    const options = state.get(this.OPTIONS);

    const page = parseInt(client.request.query.page ?? "1") - 1;
    const offset = page * options.limit;
    const result = await ORM.readAll( model, Object.assign({database: database}, options, {offset: offset}));

    state.set(this.COUNT, await ORM.count(client.model, {database: database}))
    state.set(this.INSTANCES, (!result) ? [] : [result].flat());
  }

  static async action_read(state){
    const client = state.get(ControllerMixin.CLIENT);
    const id = client.request.params?.id ? parseInt(client.request.params.id) : null;
    const database = state.get(ControllerMixinDatabase.DATABASES).get(state.get(this.DATABASE_KEY));
    state.set(this.INSTANCE, await ORM.factory( client.model, id, {database: database}));
  }

  static async action_edit(state){
    await this.action_read(state);
  }
}

module.exports = ControllerMixinORMRead;