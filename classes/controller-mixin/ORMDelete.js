const {KohanaJS, ControllerMixin, ORM, dereference:$} = require('kohanajs');
const {HelperCrypto} = require('@kohanajs/mod-crypto');

class ControllerMixinORMDelete extends ControllerMixin{
  static DELETE_SIGN = "deleteSign";
  static DELETE_DATABASE = "deleteDatabase";
  static INSTANCE = "instance";

  static async action_delete(state){
    const client = state.get('client');
    const request = client.request;
    const id = request.params.id;
    const model = state.get('model');

    if(!id) throw new Error(`Delete ${model.name} require object id`);

    const deleteKey = KohanaJS.config.database.deleteKey;
    const value = model.tableName + id;
    state.set(this.DELETE_SIGN , (deleteKey) ? await HelperCrypto.sign(deleteKey, value) : "true");

    const confirm = request.query['confirm'];
    if(!confirm)return;

    const verify = (deleteKey) ? await HelperCrypto.verify(KohanaJS.config.database.deleteKey, confirm, value) : true;
    if(!verify)throw new Error('Invalid delete signature.')

    const m = await ORM.factory(model, id, {database: state.get(this.DELETE_DATABASE)});
    state.set('instance', m);
    await m.delete();
  }
}

module.exports = ControllerMixinORMDelete;