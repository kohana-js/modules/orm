const {ControllerMixin, ORM, ControllerMixinDatabase} = require('kohanajs');
const ControllerMixinORMInput= require('./ORMInput');

class ControllerMixinORMWrite extends ControllerMixin{
  static DATABASE_KEY = 'ormDatabaseKey';
  static INSTANCE = 'instance';

  static init(state){
    state.set(this.DATABASE_KEY, state.get(this.DATABASE_KEY) || 'admin');
  }

  /**
   * @param client
   * @param Model
   * @param database
   * @param {function | object} ORMInput
input example:
Map(2) {
  'Variant' => Map(1) {
    '3818858229520' => Map(10) {
      'product_id' => '2241418003820',
      'title' => '5000cl',
      'price' => '100000',
      'compare_at_price' => '',
      'taxable' => 'FALSE',
      'sku' => '',
      'barcode' => '',
      'inventory_policy' => '',
      'requires_shipping' => '1',
      'weight' => ''
    }
  },
  'Inventory' => Map(1) {
    '?' => Map(2) {
      '.variant_id:Variant' => '3818858229520',
      'quantity' => '1000'
    }
  }
}

   */

  /**
   *
   * @param state
   * @returns {Promise<void>}
   */
  static async action_update(state){
    const client = state.get(ControllerMixin.CLIENT);
    const request = client.request;
    const id = request.params.id;
    const database = state.get(ControllerMixinDatabase.DATABASES).get(state.get(this.DATABASE_KEY));

    const input = state.get(ControllerMixinORMInput.ORM_INPUT);
    const ins = await this.#newInstance(input, client.model, database);
    //assign new created instance to client
    await this.#newChild(input, ins, database);

    await this.#updateInstances(input, id, database);
    if(!state.get(this.INSTANCE)){
        state.set(this.INSTANCE, ins)
    }
  }

  /**
   *
   * @param {Map} input
   * @param {ORM} model
   * @param database
   */
  static async #newInstance(input, model, database){
    const mapNewInstance = input.get(model.name).get('?');
    if(!mapNewInstance)return null;

    const instance = ORM.create(model, {database: database});
    mapNewInstance.forEach((v, key)=>{
      if(Array.isArray(v))return;
      instance[key] = v;
      mapNewInstance.delete(key);
    })
    await instance.write();

    //add many to many
    await Promise.all(
      Array.from(mapNewInstance.entries()).map(async v => {
        const key = v[0];
        if(!/^[*:]/.test(key) )return;

        const ModelB = ORM.require(key.slice(1))
        const models = v[1].filter(id => id !== 'replace').map(id => new ModelB(id, {database : database}));
        if(models.length === 0 )return;
        await instance.add(models);
        mapNewInstance.delete(key);
      })
    );

    //remove used map
    input.get(model.name).delete('?');

    return instance;
  }

  /**
   *
   * @param {Map} input
   * @param {Number | String} id
   * @param database
   * @returns {Promise<void>}
   */
  static async #updateInstances(input, id, database){
    await Promise.all(
      Array.from(input.entries()).map(async x =>{
        //this type is empty, quit;
        if(x[1].size === 0)return;

        const Model = ORM.require(x[0]);
        const ids = Array.from(x[1].keys());

        const results = await ORM.readBy(Model, 'id', ids , {database: database, asArray : true});

        await Promise.all(
          results.map(async m => {
            if(m.id.toString() === id){
//              this.instance = m;
              m.snapshot();
            }
            const newValues = x[1].get(m.id) ?? x[1].get(String(m.id));
            let changed = false;

            Model.fields.forEach((x, field) =>{
              const v = newValues.get(field);
              if(v === undefined)return;
              m[field] = v;
              changed = true;
            });

            Model.belongsTo.forEach((x, field) =>{
              const v = newValues.get(field);
              if(v === undefined)return;
              m[field] = v;
            });

            Model.belongsToMany.forEach(field => {
              const k = '*' + field;
              const v = newValues.get(k);
              if(v === undefined)return;
              m[k] = v;
              changed = true;
            })

            if(changed){
              const instance = new Model(m.id, {database: database});
              Object.assign(instance, m);
              await instance.write();

              const manyOps = [];
              Model.belongsToMany.forEach(x => {
                const k = `*${x}`;
                if(m[k] === undefined)return;
                if(!Array.isArray(m[k]))throw new Error(`${k} must be Array`);

                const ModelB = ORM.require(k.slice(1))
                const many = m[k].filter(id => id !== 'replace').map(id => new ModelB(id, {database: database}));
                if(many.length !== m[k].length)manyOps.push(instance.removeAll(ModelB))
                if(many.length > 0)manyOps.push(instance.add(many));
              })

              await Promise.all(manyOps);
            }
          })
        );
      })
    )
  }

  static async #newChild(input, parent, database){
    await Promise.all(
      Array.from(input.entries()).map(async x => {
        const list = x[1];
        const type = x[0];

        const Model = ORM.require(type);

        const newValues = x[1].get('?');
        if(!newValues)return;
        //the map have no fields, skip it.
        if(newValues.size <= 1)return;

        const instance = ORM.create( Model, {database: database} );

        Model.fields.forEach((x, field) =>{
          const v = newValues.get(field);
          if(!v)return;
          instance[field] = v;
          newValues.delete(field);
        });

        Model.belongsTo.forEach((ParentModel, fk)=>{
          const parentID = newValues.get(`.${fk}:${ParentModel}`)
          if(!parentID)return;
          instance[fk] = (parentID === '?') ? parent.id : parentID;
        })

        await instance.write();
        list.delete('?')
      })
    )
  }
}

module.exports = ControllerMixinORMWrite;