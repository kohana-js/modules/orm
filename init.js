const {KohanaJS} = require('kohanajs');
KohanaJS.initConfig(new Map([
  ['database', require('./config/database')],
]));